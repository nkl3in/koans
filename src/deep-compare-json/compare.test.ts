import { expect, test } from "bun:test";
import { areDeeplyEqual } from "./compare";
import { o01, o02, o11, o12, o21, o22, o31, o32 } from "./data";

test("test deep equal", () => {
  expect(areDeeplyEqual(o01, o02)).toBe(true);
  expect(areDeeplyEqual(o11, o12)).toBe(true);
  expect(areDeeplyEqual(o21, o22)).toBe(false);
  expect(areDeeplyEqual(o31, o32)).toBe(false);
});
