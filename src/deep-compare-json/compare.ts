/**
 * @param {any} o1
 * @param {any} o2
 * @return {boolean}
 */
export const areDeeplyEqual = function (o1: any, o2: any): boolean {
  let result = true;

  const compare = (_o1: any, _o2: any) => {
    if (
      /* prevent comparison of Array to Object */
      (Array.isArray(_o1) && !Array.isArray(_o2)) ||
      (Array.isArray(_o2) && !Array.isArray(_o1))
    ) {
      result = false;
    } else if (_o1 instanceof Object && _o2 instanceof Object) {
      /* Object or Array Types */
      for (const [key, val] of Object.entries(_o1)) {
        compare(val, _o2[key]);
      }
    } else if (_o1 !== _o2) {
      /* primitive */
      result = false;
    }
  };

  compare(o1, o2);
  compare(o2, o1);
  return result;
};
