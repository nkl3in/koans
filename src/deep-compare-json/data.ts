export const o01 = { x: 1, y: 2 };
export const o02 = { x: 1, y: 2 };

export const o11 = { y: 2, x: 1 };
export const o12 = { x: 1, y: 2 };

export const o21 = { x: null, L: [1, 2, 3] };
export const o22 = { x: null, L: ["1", "2", "3"] };

export const o31 = true;
export const o32 = false;
