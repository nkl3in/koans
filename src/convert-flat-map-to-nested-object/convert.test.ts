import { expect, test } from "bun:test";
import { input, output } from "./data";
import { convert } from "./convert";

test("test convert", () => {
  expect(convert()).toEqual(output);
});
