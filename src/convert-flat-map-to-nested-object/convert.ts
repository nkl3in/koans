import { input } from "./data";

type Level<T> = {
  level: number;
  num: string;
  text: string;
  children?: Array<T>;
};
interface Node extends Level<Node> {}
type Item = {
  level: number;
  num: string;
  text: string;
};

export function convert() {
  const result: Node[] = [];
  let target: Node[];
  let prevLevel: number = 0;

  input.map((item: Item, idx) => {
    const { level, num, text } = item;

    if (level === 1) {
      /**
       * The level is reset to 1:
       * - reset target to point to the result array
       */
      target = result;
    } else if (level > prevLevel) {
      /**
       * The level is incremented:
       * - set parent to the last element of target
       * - create a children array on this last element
       * - set target to point to this new empty array
       * - the new value will be pushed onto the target array
       **/
      const parent = target[target.length - 1];
      parent.children = [];
      target = parent.children;
    } else if (level === prevLevel) {
      /**
       * The level remains the same:
       * - nothing to do here
       * - the target is already pointing at the correct array
       * - the new value will be pushed onto the target array
       */
    }
    target.push({ level, num, text });
    prevLevel = item.level;
  });
  return result;
}
