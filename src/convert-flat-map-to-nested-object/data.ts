export const input = [
  { level: 1, num: "a", text: "Right to life" },
  { level: 2, num: "i", text: "Right to life protected" },
  { level: 2, num: "ii", text: "Circumstances allowing …" },
  { level: 3, num: "1", text: "Force no more than …" },
  { level: 1, num: "b", text: "Prohibition on torture" },
];

export const output = [
  {
    level: 1,
    num: "a",
    text: "Right to life",
    children: [
      { level: 2, num: "i", text: "Right to life protected" },
      {
        level: 2,
        num: "ii",
        text: "Circumstances allowing …",
        children: [{ level: 3, num: "1", text: "Force no more than …" }],
      },
    ],
  },
  { level: 1, num: "b", text: "Prohibition on torture" },
];
