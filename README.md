# koans

A collection of JS/TS puzzles

To install dependencies:

```bash
bun install
```

To run:

```bash
bun run dev
```

To test:

```
bun run test
```

This project was created using `bun init` in bun v0.5.9. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
